# Documentation

## Summary

- [*QuickSheets*](#quicksheets)
- [*Dev-ops*](#dev-ops)

## QuickSheets

- Stop all containers, removing then up (will not re built images)

```
docker stop $(docker ps -a -q) ;docker rm $(docker ps -a -q);
docker-compose up;
docker-compose up -d;
docker-compose build --no-cache;
```

## Dev-ops

Build and push an image to the azure container registery

```
$ docker login gpgkubreg.azurecr.io
#username: gpgkubreg
#password: Ya257=ah7iPMAzUPHYyQjS0h2FkCSBMx
$ docker build -t lumen . --no-cache
$ docker tag lumen gpgkubreg.azurecr.io/lumen
$ docker push gpgkubreg.azurecr.io/lumen
```
